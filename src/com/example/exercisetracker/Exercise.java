package com.example.exercisetracker;

public class Exercise { //simple exercise class to hold Parse object fields
	
	private String id;
	private String name;
	private int calorieBurn;
	
	Exercise(String exId, String exName, int exCal){
		id = exId;
		name = exName;
		calorieBurn = exCal;
	}
	
	public String getId(){
		return id;
	}
	public String getName(){
		return name;
	}
	public int getCalorieBurn(){
		return calorieBurn;
	}
	
	public void setId(String id){
		this.id = id;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setCalorieBurn(int calorieBurn){
		this.calorieBurn = calorieBurn;
	}
	
	@Override
	public String toString(){
		return this.getName();
	}
}