
package com.example.exercisetracker;



import java.util.Calendar;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphView.LegendAlign;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.LineGraphView;
import com.jjoe64.graphview.GraphView.GraphViewData;

import android.support.v7.app.ActionBarActivity;
import android.graphics.Color;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;


public class ProgressReportActivity extends ActionBarActivity {
	
	public static final String PREFS = "profile";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_progress_report);
		//Calendar
		Calendar c = Calendar.getInstance(); 
		
		SharedPreferences sp = getSharedPreferences(PREFS,0);
		Editor editor = sp.edit();
		
		
		//Pulling in the amount of calories to be burned each day and the amount that was burned by user
		String caloriesBurnt= sp.getString("calBurnTotal", "0");
		String amtToBurnDay = sp.getString("amtToBurnDayGraph", "0");
		
		//Get day of the week
		int day = c.get(Calendar.DAY_OF_WEEK);
		
		//For storing graph information
		int sunday,monday,tuesday,wednesday,thursday,friday,saturday;
		int sundayGoal,mondayGoal,tuesdayGoal,wednesdayGoal,thursdayGoal,fridayGoal,saturdayGoal;
		
		//Each day graph is accessed it updates and saves that day so that when you access the graph on other days it doesn't clear old data
		switch(day){
			case 1: editor.putString("sunday", caloriesBurnt);
					editor.putString("sundayGoal", amtToBurnDay);
					editor.commit();
			break;
			case 2: editor.putString("monday", caloriesBurnt);
					editor.putString("mondayGoal",amtToBurnDay);
					editor.commit();
			break;
			case 3: editor.putString("tuesday", caloriesBurnt);
					editor.putString("tuesdayGoal", amtToBurnDay);
					editor.commit();
			break;
			case 4: editor.putString("wednesday", caloriesBurnt);
					editor.putString("wednesdayGoal", amtToBurnDay);
					editor.commit();
			break;
			case 5: editor.putString("thursday", caloriesBurnt);
					editor.putString("thursdayGoal", amtToBurnDay);
			editor.commit();
			break;
			case 6: editor.putString("friday", caloriesBurnt);
					editor.putString("fridayGoal", amtToBurnDay);
					editor.commit();
			break;
			case 7: editor.putString("saturday", caloriesBurnt);
					editor.putString("saturdayGoal", amtToBurnDay);
					editor.commit();
			break;
		}
		
		//getting data for each day from switch
		sunday = Integer.parseInt(sp.getString("sunday", "0"));
		monday = Integer.parseInt(sp.getString("monday", "0"));
		tuesday = Integer.parseInt(sp.getString("tuesday", "0"));
		wednesday = Integer.parseInt(sp.getString("wednesday", "0"));
		thursday = Integer.parseInt(sp.getString("thursday", "0"));
		friday = Integer.parseInt(sp.getString("friday", "0"));
		saturday = Integer.parseInt(sp.getString("saturday", "0"));
		
		//Array length
		int num = 8;
	
		//Data array for line 1
		GraphViewData[] data1 = new GraphViewData[num];
		
		//Storing data for line 1 (GraphViewData(int x, int y)
		data1[0] = new GraphViewData(1, 0);
		data1[1]= new GraphViewData(2, sunday);
		data1[2]= new GraphViewData(3, monday);
		data1[3]= new GraphViewData(4, tuesday);
		data1[4]= new GraphViewData(5, wednesday);
		data1[5]= new GraphViewData(6, thursday);
		data1[6]= new GraphViewData(7, friday);
		data1[7]= new GraphViewData(8, saturday);
	
		//Making new line with redish color
		GraphViewSeries line1 = new GraphViewSeries("You", new GraphViewSeriesStyle(Color.rgb(200, 50, 00), 3), data1);
		
		//getting data for goal from switch
		sundayGoal = Integer.parseInt(sp.getString("sundayGoal", "0"));
		mondayGoal = Integer.parseInt(sp.getString("mondayGoal", "0"));
		tuesdayGoal = Integer.parseInt(sp.getString("tuesdayGoal", "0"));
		wednesdayGoal = Integer.parseInt(sp.getString("wednesdayGoal", "0"));
		thursdayGoal = Integer.parseInt(sp.getString("thursdayGoal", "0"));
		fridayGoal = Integer.parseInt(sp.getString("fridayGoal", "0"));
		saturdayGoal = Integer.parseInt(sp.getString("saturdayGoal", "0"));
		
		//Data for line 2
		GraphViewData[] data2 = new GraphViewData[num];
		data2[0] = new GraphViewData(1, 0);
		data2[1]= new GraphViewData(2, sundayGoal);
		data2[2]= new GraphViewData(3, mondayGoal);
		data2[3]= new GraphViewData(4, tuesdayGoal);
		data2[4]= new GraphViewData(5, wednesdayGoal);
		data2[5]= new GraphViewData(6, thursdayGoal);
		data2[6]= new GraphViewData(7, fridayGoal);
		data2[7]= new GraphViewData(8, saturdayGoal);
		
		
		//Making new line with greenish color
		GraphViewSeries line2 = new GraphViewSeries("Goal",new GraphViewSeriesStyle(Color.rgb(90, 250, 00), 3), data2);
		
		
			GraphView graphView = new LineGraphView(
			    this // context
			    , "Progress Report (calories per day)" // heading
			);
			 
			//Set the number of labels for graph
			graphView.getGraphViewStyle().setNumHorizontalLabels(10);
			graphView.getGraphViewStyle().setNumVerticalLabels(8);
			
			//Set the labels names
			graphView.setVerticalLabels(new String[]{"2000", "1750", "1500", "1250","1000", "750", "500", "250", "0"});
			graphView.setHorizontalLabels(new String[]{" ","Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"});

			
			graphView.setManualYAxisBounds(2000, 0);

			 
			//adding data to chart
			graphView.addSeries(line1); // data
			graphView.addSeries(line2); // data
			
			// set legend
			graphView.setShowLegend(true);
			graphView.setLegendAlign(LegendAlign.TOP);
			
			graphView.setScrollable(true);
			 
			//Sending to linear layout
			LinearLayout layout = (LinearLayout) findViewById(R.id.progress);
			layout.addView(graphView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.progress_report, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_Home) {
			goHome();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//Goes Back to Home
		public void goHome(){
			Intent newActivity = new Intent(this, HomeScreenActivity.class);
	    	startActivity(newActivity);
		}
}


