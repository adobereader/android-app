package com.example.exercisetracker;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MyGoalActivity extends ActionBarActivity {

	//Create objects
	TextView myGoalTitle, myGoalWeight, myGoalWeekPounds, myGoalNotes;
	String goalTitle, goalWeight, goalWeekPounds, goalNotes;
	
	public static final String PREFS = "profile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_goal);
		
		SharedPreferences sp = getSharedPreferences(PREFS,0);
		
		//instantiate 
		myGoalTitle =  (TextView) findViewById(R.id.MyGoalTitle);
		myGoalWeight =  (TextView) findViewById(R.id.MyGoalWeight);
		myGoalWeekPounds =  (TextView) findViewById(R.id.MyGoalDate);
		myGoalNotes =  (TextView) findViewById(R.id.MyGoalNotes);
		
		//Get from shared preferences
		goalTitle=sp.getString("goalTitle", "N/A");
		goalWeight=sp.getString("goalWeight", "N/A");
		goalWeekPounds=sp.getString("weekPounds","1");
		goalNotes=sp.getString("goalNotes", "N/A");
	
		//Write to textviews
		myGoalTitle.setText(goalTitle);
		myGoalWeight.setText(goalWeight);
		myGoalWeekPounds.setText(goalWeekPounds);
		myGoalNotes.setText(goalNotes);
		
		
		
		
	}

	//Handles populating Menu button and or Actionbar
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_goals, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_Home) {
			goHome();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//Goes Back to Home
		public void goHome(){
			Intent newActivity = new Intent(this, HomeScreenActivity.class);
	    	startActivity(newActivity);
		}
	
	public void sendMessage(View view){
    	Intent newActivity = new Intent(this, SetGoalActivity.class);
    	startActivity(newActivity);
	}

}
