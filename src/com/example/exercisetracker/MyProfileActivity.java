package com.example.exercisetracker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class MyProfileActivity extends ActionBarActivity {
	public final static String EXTRA_MESSAGE = "nothing";
	
	//Create View objects
	TextView name, age, ft, in, currentWeight;
	Spinner spActiveLevel;
	Button save;
	RadioButton rdo;
	
	//Variables for Shared Preferences from edit profile
	String strName, strAge, strFt, strIn, strCurrentWeight, strGender, strActiveLevel;
	
	
	public static final String PREFS = "profile";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.actvitiy_my_profile);
	
		
    	SharedPreferences sp = getSharedPreferences(PREFS, 0);
		
		//link references to xml View by typecasting
		name = (TextView) findViewById(R.id.txtViewName);
		age = (TextView) findViewById(R.id.txtViewAge);
		ft = (TextView) findViewById(R.id.txtViewFeet);
		in = (TextView) findViewById(R.id.txtViewInches);
		currentWeight = (TextView) findViewById(R.id.txtViewCurrentWeight);
		
		//Pulling from edit profile
		strName=sp.getString("name", "N/A");
		strAge=sp.getString("age", "0");
		strFt=sp.getString("ft", "5");
		strIn=sp.getString("in","4");
		strCurrentWeight=sp.getString("weight","155");
	
		//Setting texts
		name.setText(strName);
		age.setText(strAge);
		ft.setText(strFt);
		in.setText(strIn);
		currentWeight.setText(strCurrentWeight);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_Home) {
			goHome();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	//Goes Back to Home
		public void goHome(){
			Intent newActivity = new Intent(this, HomeScreenActivity.class);
	    	startActivity(newActivity);
		}
	
	/*Button Save and Continue*/
    public void sendMessage(View v){

    	strActiveLevel = ((Spinner) findViewById(R.id.spinActiveLevel)).getSelectedItem().toString();
    	Intent newActivity = new Intent(this, ProfileActivity.class);
    	startActivity(newActivity);
    }

}
