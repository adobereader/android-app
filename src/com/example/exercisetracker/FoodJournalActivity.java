package com.example.exercisetracker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class FoodJournalActivity extends ListActivity { //named it this to avoid a conflict
	
	private List<FoodItem> foodItems; //will hold the list of food from the Parse database
	public int calorieTotal = 0; //will hold the accumulating calorie total
	TextView totalCalories; //textview the calorie total will go in to
	
	public static final String PREFS = "profile";
	int calEatenTotal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		Parse.initialize(this, "wQXktsAd8IVZF8J08czan0CHzhLilk6OsnckBMHn", "D2QObY0jVPL9DrSO9L9E3viytudd53MRelIbzqd7");
		setContentView(R.layout.activity_food_journal);
		
		
		
		totalCalories = (TextView)findViewById(R.id.totalCalories);
		foodItems = new ArrayList<FoodItem>();
		ArrayAdapter<FoodItem> fdAdapter = new ArrayAdapter<FoodItem>(this, android.R.layout.simple_list_item_1, foodItems); //adapts data model to list view
		setListAdapter(fdAdapter);
		refreshFoodList();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.food_journal, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_Home) {
			goHome();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//Goes Back to Home
		public void goHome(){
			Intent newActivity = new Intent(this, HomeScreenActivity.class);
	    	startActivity(newActivity);
		}

	private void refreshFoodList() { //fetches data from Parse and assigns it to foodItems list
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Food");
		query.orderByAscending("Name");
		setProgressBarIndeterminateVisibility(true);
		query.findInBackground(new FindCallback<ParseObject>(){ //fetches data on background thread

			@Override
			public void done(List<ParseObject> foodList, ParseException e) {
				setProgressBarIndeterminateVisibility(false);
				if(e == null){
					foodItems.clear();
					for(ParseObject food : foodList){ //update list
						FoodItem foodItem = new FoodItem(food.getObjectId(), food.getString("Name"), food.getInt("Calories"));
						foodItems.add(foodItem);
					}
					((ArrayAdapter<FoodItem>) getListAdapter()).notifyDataSetChanged(); //notify adapter
				}else{
					Log.d(getClass().getSimpleName(), "Error: " + e.getMessage());
				}	
			}
		});
	}
	
	//will add the food item's calories to running total when clicked
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id){
		FoodItem foodItem = foodItems.get(position);
		calorieTotal += foodItem.getCalories();
		totalCalories.setText("Calories: " + String.valueOf(calorieTotal));
	}
	
	public void save(View view){
		//Preferences
		SharedPreferences sp = getSharedPreferences(PREFS, 0);
		Editor editor = sp.edit();
		//Set the total calories eaten for the day to the total eaten during this activitiy
		calEatenTotal=Integer.parseInt(sp.getString("calTotal", "0"))+calorieTotal;
		
		//Get the calories from activity and send it to preferences
		String calTotal = String.valueOf(calorieTotal);
    	editor.putString("calories", calTotal);
    	
    	//Send total for the day to preferences
    	editor.putString("calTotal", String.valueOf(calEatenTotal));
    	editor.commit();
    	
    	Intent newActivity = new Intent(this, HomeScreenActivity.class);
    	startActivity(newActivity);
		
	}
}