package com.example.exercisetracker;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class ProfileActivity extends ActionBarActivity{
	public final static String EXTRA_MESSAGE = "nothing";
	int height = 0;
	
	//Create View objects
	EditText name, age, ft, in, currentWeight;
	RadioGroup rgGender;
	RadioButton rbFemale;
	RadioButton rbMale;
	Spinner spActiveLevel;
	Button save;
	RadioButton rdo;
	
	//Variables for sending to MyProfile
	String strName, strAge, strFt, strIn, strCurrentWeight, strGender, strActiveLevel;
	
	
	public static final String PREFS = "profile";
	 /*
	  * Method Name: onCreate
	  * Purpose:
	  */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		
		//link references to xml View by typecasting
		name = (EditText) findViewById(R.id.txtName);
		age = (EditText) findViewById(R.id.txtAge);
		ft = (EditText) findViewById(R.id.txtFeet);
		in = (EditText) findViewById(R.id.txtInches);
		currentWeight = (EditText) findViewById(R.id.txtCurrentWeight);

	
		//Validate Name
		name.addTextChangedListener(new TextWatcher(){
			//WILL NOT USE
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
				
			}
			//WILL NOT USE
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (Validation.validName(name)){
					strName = name.getText().toString();
				}
				else{
					strName= null;
				}
			}
		});
		
		//validate Age
		age.addTextChangedListener(new TextWatcher(){
			//WILL NOT USE
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
				
			}
			//WILL NOT USE
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (Validation.validAge(age)){
					strAge = age.getText().toString();
				}
				else{
					strAge=null;
				}
			}
		});
		
		//validate Feet
		ft.addTextChangedListener(new TextWatcher(){
			//WILL NOT USE
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
		
			}
			//WILL NOT USE
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(Validation.validFt(ft)){
					strFt = ft.getText().toString();
				}
				else{
					strFt=null;
				}
			}
			
		});
	
		//validate Inches
		in.addTextChangedListener(new TextWatcher(){
			//WILL NOT USE
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
				
			}
			//WILL NOT USE
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(Validation.validIn(in)){
					strIn = in.getText().toString();
				}
				else{
					strIn=null;
				}
			}
			
		});
		
		//validate Current Weight
		currentWeight.addTextChangedListener(new TextWatcher(){
			//WILL NOT USE
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
				
			}
			//WILL NOT USE
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(Validation.validWeight(currentWeight)){
					strCurrentWeight = currentWeight.getText().toString();
				}
				else{
					strCurrentWeight =null;
				}	
			}
		});
		
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_Home) {
			goHome();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//Goes Back to Home
	public void goHome(){
		Intent newActivity = new Intent(this, HomeScreenActivity.class);
		   startActivity(newActivity);
	}
	
	/*Button Save and Continue*/
    public void save(View v){

    	strActiveLevel = ((Spinner) findViewById(R.id.spinActiveLevel)).getSelectedItem().toString();
    	
    	
    	SharedPreferences sp = getSharedPreferences(PREFS, 0);
    	Editor editor = sp.edit();
    	//Putting data into shared preferences
    	editor.putString("name", strName);
    	editor.putString("age", strAge);
    	editor.putString("ft", strFt);
    	editor.putString("in", strIn);
    	editor.putString("weight", strCurrentWeight);
    	editor.commit();
    	
    
    	
    	
    	
    	Intent newActivity = new Intent(this, HomeScreenActivity.class);
    	startActivity(newActivity);
    }

}
    
    
    
  