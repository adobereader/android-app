package com.example.exercisetracker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class SetGoalActivity extends ActionBarActivity {

	//Create View objects
	EditText goalTitle, goalWeight, targetDate, notes, cheattxt;
	
	//Variables
	String strGoalTitle, strGoalWeight, strWeekPounds, strNotes, cheat;
	int intGoalWeight;
	//Date targDate = new Date();
	
	public static final String PREFS = "profile";
	
	/*
	  * Method Name: onCreate
	  * Purpose:
	  */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_set_goal);
		
		//link references to xml View by typecasting
		goalTitle = (EditText) findViewById(R.id.txtGoalTitle);
		goalWeight = (EditText) findViewById(R.id.txtGoalWeight);
		targetDate = (EditText) findViewById(R.id.goalDate);
		notes = (EditText) findViewById(R.id.goalNotes);
		cheattxt = (EditText) findViewById(R.id.editText1);
		

		
		
		//Validate goalTitle
		goalTitle.addTextChangedListener(new TextWatcher(){
			//WILL NOT USE
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
				
			}
			//WILL NOT USE
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (Validation.validTitle(goalTitle)){
					strGoalTitle = goalTitle.getText().toString();
				}
				else{
					strGoalTitle=null;
				}	
			}	
		});
		
		//Validate goal weight
		goalWeight.addTextChangedListener(new TextWatcher(){
			//WILL NOT USE
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
				
			}
			//WILL NOT USE
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if(Validation.validWeight(goalWeight)){
					strGoalWeight = goalWeight.getText().toString();
				}
				else{
					strGoalWeight=null;
				}
			}
			
		});
		
		//Validate date ---NOT COMPLETE
		targetDate.addTextChangedListener(new TextWatcher(){
			//WILL NOT USE
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,int after) {
				// TODO Auto-generated method stub
				
			}
			//WILL NOT USE
			@Override
			public void onTextChanged(CharSequence s, int start, int before,int count) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (Validation.validTitle(goalTitle)){
					strWeekPounds = targetDate.getText().toString();
				}
				else{
					strWeekPounds=null;
				}	
			}	
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.set_goal, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_Home) {
			goHome();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//Goes Back to Home
		public void goHome(){
			Intent newActivity = new Intent(this, HomeScreenActivity.class);
	    	startActivity(newActivity);
		}
	
	/*Button Save and Continue*/
    public void save(View v){
    	
    	cheat = cheattxt.getText().toString();//For cheating
    	

    	//Shared preferences
    	SharedPreferences sp = getSharedPreferences(PREFS, 0);
    	Editor editor = sp.edit();
    	
    	strNotes=notes.getText().toString();
    	
    	//Storing data for goal in shared preferences
    	editor.putString("calBurnTotal", cheat);
    	editor.putString("goalTitle",strGoalTitle);
    	editor.putString("goalWeight", strGoalWeight);
    	editor.putString("weekPounds",strWeekPounds);
    	editor.putString("goalNotes",strNotes);
    	editor.commit();
    	
    	
    	
    	
    	Intent newActivity = new Intent(this, HomeScreenActivity.class);
    	startActivity(newActivity);
    }
}
