package com.example.exercisetracker;

public class FoodItem { //simple foodItem class to hold Parse object fields
	
	private String id;
	private String name;
	private int calories;
	
	FoodItem(String foodId, String foodName, int foodCalories){
		id = foodId;
		name = foodName;
		calories = foodCalories;
	}
	
	public String getId(){
		return id;
	}
	public String getName(){
		return name;
	}
	public int getCalories(){
		return calories;
	}
	
	public void setId(String id){
		this.id = id;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setCalories(int calories){
		this.calories = calories;
	}
	
	@Override
	public String toString(){
		return this.getName() + " - " + this.getCalories() + " cal";
	}
}
