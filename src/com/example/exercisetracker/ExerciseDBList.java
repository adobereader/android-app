package com.example.exercisetracker;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ExerciseDBList extends ListActivity {

	private List<Exercise> workouts; //will hold the list of workouts from the Parse database
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Parse.initialize(this, "wQXktsAd8IVZF8J08czan0CHzhLilk6OsnckBMHn", "D2QObY0jVPL9DrSO9L9E3viytudd53MRelIbzqd7");
		/*
		setContentView(R.layout.activity_db_workout_list);
		
		workouts = new ArrayList<Exercise>();
		ArrayAdapter<Exercise> exAdapter = new ArrayAdapter<Exercise>(this, android.R.layout.simple_list_item_1, workouts); //adapts data model to list view
		setListAdapter(exAdapter);
		refreshExerciseList();
		*/
	}
	
	private void refreshExerciseList(){ //fetches data from Parse and assigns it to workouts list
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Workout");
		query.orderByAscending("Name");
		query.findInBackground(new FindCallback<ParseObject>(){ //fetches data on background thread

			@Override
			public void done(List<ParseObject> workoutList, ParseException e) {
				if(e == null){
					workouts.clear();
					for(ParseObject workout : workoutList){ //update list
						Exercise exercise = new Exercise(workout.getObjectId(), workout.getString("Name"), workout.getInt("CalorieBurn"));
						workouts.add(exercise);
					}
					((ArrayAdapter<Exercise>) getListAdapter()).notifyDataSetChanged(); //notify adapter
				}else{
					Log.d(getClass().getSimpleName(), "Error: " + e.getMessage());
				}
			}
		});
	}
}