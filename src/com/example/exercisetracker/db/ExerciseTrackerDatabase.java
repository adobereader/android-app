/**
 * 
 */
package com.example.exercisetracker.db;

import com.example.exercisetracker.Run;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 *Class Name:	ExerciseTrackerDatabase
 *Purpose: 		This class will extend SQLiteOpenHelper to create database tables for app.
 */
public class ExerciseTrackerDatabase extends SQLiteOpenHelper {
	
	//Database Name
	private static final String DATABASE_NAME = "ExerciseTracker.db";
	//Database Version
	private static final int DATABASE_VERSION = 1; //MUST INCREMENT IF YOU MAKE CHANGES TO ANY TABLE'S STRUCTURE
	
	//Common Column(s) that [some/most] tables will contain
	private static final String ID_COLUMN = "id";
	private static final String DATE_ADDED = "Date_Added";
	
	//RUN HISTORY TABLE///////////////////////////////////////////////////////////////////////////////////
	private static final String TABLE_RUNS ="Run_History";//Table Name
		//TABLE COLUMN NAMES
	private static final String DURATION_COLUMN = "Duration";
	private static final String MILES_COLUMN = "Miles";
	private static final String CALS_BURNED_COLUMN = "Calories_Burned";
	
	/*FOOD JOURNAL TABLE/////////////////////////////////////////////////////////////////////////////////
	private static final String TABLE_JOURNAL = "Food_Journal";
		//TABLE COLUMN NAMES
	private static final String FOOD_DES_COLUMN = "Food_Des";
	private static final String MEAL_TYPE_COLUMN ="Meal_Type";
	private static final String CALS_COLUMN ="Calories";
	private static final String DATE_CHANGED = "Date_Changed";
	
	/*GOAL TABLE////////////////////////////////////////////////////////////////////////////////////////
	private static final String TABLE_GOALS ="Goals";
		//TABLE COLUMN NAMES
	private static final String TITLE_COLUMN = "Title";
	private static final String GOAL_WEIGHT = "Goal_Weight";
	private static final String TARGET_DATE = "Target_Date";
	private static final String NOTES_COLUMN = "Notes";
	
	/*WORKOUTS TABLE////////////////////////////////////////////////////////////////////////////////////
	private static final String TABLE_WORKOUTS ="Workouts";
	//TABLE COLUMN NAMES
	private static final String NAME_COLUMN = "Name";
	private static final String WORKOUT_DURATION = "Duration";
	private static final String CALS_BURNED = "Calories_Burned";
	*/
	
	//CREATE TABLES
	//Run Table Create Statement
	private static final String CREATE_TABLE_RUNS = "CREATE TABLE " +TABLE_RUNS+ 
			" (" +ID_COLUMN+ " INTEGER PRIMARY KEY AUTOINCREMENT , "
			     +DURATION_COLUMN+ " VARCHAR(10), "
			     +MILES_COLUMN+ " REAL(4), "
			     +CALS_BURNED_COLUMN+ " INTEGER(4), " 
			     +DATE_ADDED+ " VARCHAR(12));";
	
	/*Food Journal Table Create Statement
	private static final String CREATE_TABLE_JOURNAL = "CREATE TABLE " +TABLE_JOURNAL+
			" (" +ID_COLUMN+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
				 +FOOD_DES_COLUMN+ " VARCHAR(40), "
				 +MEAL_TYPE_COLUMN+ " VARCHAR(10), "
				 +CALS_COLUMN+ " INTEGER, "
				 +DATE_ADDED+ " DATE, "
				 +DATE_CHANGED+ " DATE NULL);";
	
	/*Goal Table Create Statement ---DECIDED NOT TO DO
	private static final String CREATE_TABLE_GOALS = "CREATE TABLE " +TABLE_GOALS+
			" (" +ID_COLUMN+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
				 +TITLE_COLUMN+ " VARCHAR(20), "
				 +GOAL_WEIGHT+ " INTEGER(3), "
				 +NOTES_COLUMN+ " VARCHAR(255), "
				 +TARGET_DATE+ " DATE, "
				 +DATE_ADDED+ " DATE, "
				 +DATE_CHANGED+ " DATE NULL);";	
	
	/**
	 * Method Name:	ExerciseTrackerDatabase
	 * Purpose:		This is a constructor method
	 */
 	public ExerciseTrackerDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	
	}


	/* Method Name:	onCreate
	 * Purpose:		This method is called when the database is created for the first time
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		//create the database
		db.execSQL(CREATE_TABLE_RUNS);
		//db.execSQL(CREATE_TABLE_JOURNAL);
		//db.execSQL(CREATE_TABLE_GOALS);
		
		Log.i("Exercise Tracker", "Exercise Tracker db has been created");//success message
	}

	/* Method Name:	onUpgrade
	 * Purpose:		This method is called when the database needs to be upgraded. Will be used to drop tables, add tables, etc.
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//drop tables
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RUNS);
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOURNAL);
		//db.execSQL("DROP TABLE ID EXISTS " + TABLE_GOALS);
		
		//create new tables
		onCreate(db);
	}
	
	
	//Add run to database (like INSERT)
	public void addRun(Run aRun){
		SQLiteDatabase db = this.getWritableDatabase();//open db connection
		ContentValues values = new ContentValues();
		//get values and place in columns
		values.put(DURATION_COLUMN, aRun.getRunDuration());
		values.put(MILES_COLUMN, aRun.getMilesRan());
		values.put(CALS_BURNED_COLUMN, aRun.getCaloriesBurned());
		values.put(DATE_ADDED, aRun.getDateOfRun());
		
		//long id = db.insert(TABLE_RUNS, null, values); //returns the id of the row in the table if successful
		db.insert(TABLE_RUNS, null, values);
		Log.i("AddRun", "Data Saved");//success message
		
		//close database
		db.close();
	}

}
