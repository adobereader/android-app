package com.example.exercisetracker;

import android.widget.EditText;

/*
 * Class Name: Validation.java
 * Purpose:		This class will hold all the data validation methods for the application
 */
public abstract class Validation {

	/*
	 * Method name: ValidName
	 * Purpose:		To validate that user entered a name
	 * Parameter: 	EditText
	 * Return: 		boolean 
	 */
	public static boolean validName(EditText e){
		
		if (e.getText().toString().length() == 0){
			e.setError("Must enter Name");
			return false;
		}
		else if (e.getText().toString().length() < 2){
			e.setError("Name Invalid");
			return false;
		}
		else{
			e.setError(null);
			return true;
		}
	}
	
	/*
	 * Method name: ValidAge
	 * Purpose:		To validate that user is of a appropriate age to use app
	 * Parameter: 	EditText
	 * Return: 		boolean 
	 */
	public static boolean validAge(EditText e){
		
		if (e.getText().toString().length() == 0){
			e.setError("Must enter an age");
			return false;
		}
		else if (Integer.valueOf(e.getText().toString()) < 15 || Integer.valueOf(e.getText().toString()) >70){
			e.setError("Must be older than 15\nMust be no older than 70");
			return false;
		}
		else{
			e.setError(null);
			return true;
		}
	}
	
	/*
	 * Method name: ValidFt
	 * Purpose:		To validate that user number of feet is valid
	 * Parameter: 	EditText
	 * Return: 		boolean 
	 */
	public static boolean validFt(EditText e){
		
		if (e.getText().toString().length() == 0){
			e.setError("Must enter foot measurement");
			return false;
		}
		else if (Integer.valueOf(e.getText().toString()) < 4 || Integer.valueOf(e.getText().toString()) > 7){
			e.setError("Invalid foot measurement");
			return false;
		}
		else{
			e.setError(null);
			return true;
		}	
	}
	
	/*
	 * Method name: ValidIn
	 * Purpose:		To validate that user number of inches is valid
	 * Parameter: 	EditText
	 * Return: 		boolean
	 */
	public static boolean validIn(EditText e){
		
		if (e.getText().toString().length() == 0){
			e.setError("Must enter inches");
			return false;
		}
		else if (Integer.valueOf(e.getText().toString()) > 11){
			e.setError("Inches Invalid");
			return false;
		}
		else{
			e.setError(null);
			return true;
		}
	}
	
	/*
	 * Method name: ValidWeight
	 * Purpose:		To validate that user's weight is valid
	 * Parameter: 	EditText
	 * Return: 		boolean
	 */
	public static boolean validWeight(EditText e){
		
		if (e.getText().toString().length() == 0){
			e.setError("Must enter weight.");
			return false;
		}
		else if (Integer.valueOf(e.getText().toString()) < 100 || Integer.valueOf(e.getText().toString()) > 400){
			e.setError("Invalid Weight");
			return false;
		}
		else{
			e.setError(null);
			return true;
		}
	}
	
	/*
	 * Method name: ValidTitle
	 * Purpose:		To validate that user's Goal Title
	 * Parameter: 	EditText
	 * Return: 		boolean
	 */
	public static boolean validTitle(EditText e){
		
		if (e.getText().toString().length() == 0){
			e.setError("Must enter Title");
			return false;
		}
		else{
			e.setError(null);
			return true;
		}
	}
}
