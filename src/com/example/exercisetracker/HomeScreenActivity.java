package com.example.exercisetracker;

import java.util.Calendar;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class HomeScreenActivity extends ActionBarActivity {

	public static final String PREFS = "profile";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_screen);
		
		//Calendar constructor
		Calendar c = Calendar.getInstance();
		
		
		//Shared Preferences
		SharedPreferences sp = getSharedPreferences(PREFS, 0);
		Editor editor = sp.edit();
		
		//Pulling data for calculation Calorieseaten/weekpounds/calories burnt. This helps calculate the amount of calories you need to burn each day
		String caloriesEaten = sp.getString("calTotal", "0");
		String weekPounds = sp.getString("weekPounds", "1");
		String caloriesBurnt= sp.getString("calBurnTotal", "0");
		
		//Parsing
		int intWeekPounds = Integer.parseInt(weekPounds);
		int intCaloriesEaten = Integer.parseInt(caloriesEaten);
		int intCaloriesBurnt = Integer.parseInt(caloriesBurnt);
		int amountDayCal=0;
		

		//Getting the amount of pounds lost per week and multiplying it by 3500 then /7 to get each day
    	int amountDay =(intWeekPounds*3500)/7;
    	//If you eat more then 2500 calories they will be added to the amount you have to lose for that day
    	if(intCaloriesEaten>=2500){
    		amountDay = amountDay+(intCaloriesEaten-2500);
    	}
    	//Taking burnt calories away from calories needed to lose
    	if(amountDay>0){
    	amountDayCal=amountDay-intCaloriesBurnt;
    	}
    	//Parse
    	String strAmountDay = String.valueOf(amountDayCal);
    	editor.putString("amtToBurnDay", strAmountDay);
    	editor.putString("amtToBurnDayGraph", String.valueOf(amountDay));
    	editor.commit();
		
    	
    	
		//Setting the day to current days date
		int day=Integer.parseInt(sp.getString("today","342"));
		
		//If the day in day var isnt equal to the current day
		if(day!=c.get(Calendar.DAY_OF_YEAR))
		{
			//Resets date to today and stores it so that day=today
			int today = c.get(Calendar.DAY_OF_YEAR);
			editor.putString("today", String.valueOf(today));
			//Sets universal variables to 0
			editor.putString("calTotal", "0");
			editor.putString("distance", "0");
			editor.putString("calBurnTotal","0");
			editor.putString("amtToBurnDayGraph", "500");
			editor.commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_screen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/*Button launchStartWorkout*/
    public void launchStartWorkout(View v){
    	Intent newActivity = new Intent(this, StartWorkoutActivity.class);
    	startActivity(newActivity);
    }
    
    /*Button launchRunTracker*/
    public void launchRunTracker(View v){
    	Intent newActivity = new Intent(this, RunTrackerActivity.class);
    	startActivity(newActivity);
    }
    
    /*Button launchFoodJournal*/
    public void launchFoodJournal(View v){
    	Intent newActivity = new Intent(this, FoodJournalActivity.class);
    	startActivity(newActivity);
    }
    
    /*Button launchMyProfile*/
    public void launchMyProfile(View v){
    	Intent newActivity = new Intent(this, MyProfileActivity.class);
    	startActivity(newActivity);
    }
    
    /*Button launchProgressReport*/
    public void launchProgressReport(View v){
    	Intent newActivity = new Intent(this, ProgressReportActivity.class);
    	startActivity(newActivity);
    }
    
    /*Button launchMyGoals*/
    public void launchMyGoals(View v){
    	Intent newActivity = new Intent(this, MyGoalActivity.class);
    	startActivity(newActivity);
    }
}
