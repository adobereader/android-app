package com.example.exercisetracker;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class SplashActivity extends Activity {

	//Splash Timer
	private static int SPLASH_TIME = 3000;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		//THREAD FOR TIMER
		Thread splash = new Thread(){
			public void run(){
				try{
					
					sleep(SPLASH_TIME);
					
					//Start the Next Activity
					startNewActivity();
					
					
					//end this activity
					finish();
					
				}catch(Exception e){
					
				}
			
			}
		};
		
		//start splash
		splash.start();
		
	}//end of OnCreate
	
	/*
	 * Method Name: startNew Activity
	 * Purpose:		direct to appropriate activity if user is not new
	 */
	public void startNewActivity(){
		
		Intent newActivity = new Intent(this, HomeScreenActivity.class);
			    	startActivity(newActivity);

		
			
	}
	
	
}//end of class
