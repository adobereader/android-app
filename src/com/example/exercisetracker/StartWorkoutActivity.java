package com.example.exercisetracker;

import java.util.Calendar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;




public class StartWorkoutActivity extends ActionBarActivity{

	TextView caloriesEat, amtToBurn, calBurnt, distLength;
	public static final String PREFS = "profile";
	int calEat;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_workout);
		
		//SharedPreferences
		SharedPreferences sp = getSharedPreferences(PREFS,0);
		Editor editor = sp.edit();
		
		//Textview Assignments
		caloriesEat = (TextView) findViewById(R.id.amtOfCals);
		amtToBurn = (TextView) findViewById(R.id.amtToBurn);
		calBurnt = (TextView) findViewById(R.id.calBurnt);
		distLength = (TextView) findViewById(R.id.distTravel);
		
		//Pulling data from Preferences
		String strAmountDay = sp.getString("amtToBurnDay", "0");
		String caloriesEaten = sp.getString("calTotal", "0");
		String caloriesBurnt= sp.getString("calBurnTotal", "0");
		String distance=sp.getString("distance","0.00");
    	
		//Set Text
    	calBurnt.setText(caloriesBurnt);
    	distLength.setText(distance);
		caloriesEat.setText(caloriesEaten);
		amtToBurn.setText(strAmountDay);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start_workout, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_Home) {
			goHome();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	//Goes Back to Home
	public void goHome(){
		Intent newActivity = new Intent(this, HomeScreenActivity.class);
    	startActivity(newActivity);
	}
	
	
	//Launches Run Tracker
	public void sendMessage(View view){
    	Intent newActivity = new Intent(this, RunTrackerActivity.class);
    	startActivity(newActivity);
	}

}
		
		
		
		