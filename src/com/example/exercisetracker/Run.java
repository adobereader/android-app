package com.example.exercisetracker;
import java.util.Date;

/**
 * 
 */

/**
 * Class Name:	Run.java
 * Purpose:		Hold all attributes for Run
 */
public class Run {
	
	//Attributes
	private int runID;
	private String runDuration;
	private double milesRan;
	private int calsBurned;
	private String dateOfRun;
	
	//Constructor
	public Run (String strDuration, double dblmiles, int intCals, String strRunDate){
		runDuration = strDuration;
		milesRan = dblmiles;
		calsBurned = intCals;
		dateOfRun = strRunDate;
	}
	
	//Getters
	//get run id
	public int getRunId(){
		return runID;
	}
	
	//get Run Duration
	public String getRunDuration (){
		return runDuration;
	}
	
	//get miles
	public double getMilesRan(){
		return milesRan;
	}
	
	//get calories burned from run
	public int getCaloriesBurned (){
		return calsBurned;
	}
	
	//get date of run
	public String getDateOfRun(){
		return dateOfRun;
	}
	
	//Setters
	//set run duration
	public void setRunDuration (String durationInput){
		runDuration = durationInput;
	}
	
	//set miles ran
	public void setMilesRan(double milesInput){
		milesRan = milesInput;
	}
	
	//set number of calories burned
	public void setCaloriesBurned(int calsInput){
		calsBurned= calsInput;
	}
	
	//set date of run
	public void setRunDate(String dateInput){
		dateOfRun = dateInput;
	}
	
}
