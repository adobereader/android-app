package com.example.exercisetracker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.example.exercisetracker.db.ExerciseTrackerDatabase;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RunTrackerActivity extends ActionBarActivity implements SensorEventListener{

	//Accelerometer declarations for setup
		Sensor accelerometer;
		SensorManager sm;
		
		//Text views for setting UI elements
		TextView speed;
		TextView distance;
		TextView calories;
		TextView timer;
		
		//Results of Distance/Speed/Calories. You must .valueOf numbers into a string in order to send it to TextBox
		String resultDistance, resultSpeed ;
		
		//Number values for math/step calculation
		int vector;						//Vector in order to reduce false step counting
		int steps; 						//Number of steps taken based on X/Y/Z and Accelerometer
		int minVector;					//Setting the Min/Max vector for step detection to an unreachable number for pausing Pedometer (Not the most efficient way ever)
		int maxVector=100;				//See above
		int height = 72; 				//Height needed for calculating step distance (Set at 72 until we have user input)
			
		//Distance Tracking
		final int mile = 5280;			//length of a mile in feet needed for step distance calculation
		double distanceMiles=0;			//Length that the person has traveled in miles since start of Pedometer
		
		//Speed Travelling
		double filter = 5;				//Number of seconds till MPH updates (5 works best)
		double distanceMilesTemp;		//DistanceMiles used for calculating speed during 5 second interval
		double distanceMilesOld;		//DistanceMiles added up except for the current iteration of the If statement
		double mphTotal= 0;				//Total MPH

		//For calorie burn calculation
		int caloriesBurned=0;
		int size=0;
		String profileData;

		//For shared preferences
		public static final String PREFS = "profile";
		String name;
		int age, ft, in, weight;
		
		//For Save button
		int caloriesBurnt;
		double distanceTotal;
		
		//DataHandler handler;

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_run_tracker);
			
			//getting the accelerometer data from phone sensors
			sm=(SensorManager)getSystemService(SENSOR_SERVICE);
			accelerometer=sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
			
			//setting Speed/distance/Timer/Calorie text boxes
			speed=(TextView)findViewById(R.id.speed);
			distance=(TextView)findViewById(R.id.distance);
			timer=(TextView)findViewById(R.id.timer);
			calories=(TextView)findViewById(R.id.calories);

			SharedPreferences sp = getSharedPreferences(PREFS,0);
			
			//Pulling from sharedPreferences
			name=sp.getString("name", "N/A");
			age=Integer.parseInt(sp.getString("age", "0"));
			ft=Integer.parseInt(sp.getString("ft", "5"));
			in=Integer.parseInt(sp.getString("in","4"));
			weight=Integer.parseInt(sp.getString("weight","155"));
			
			//Converting feet into inches then adding for height
			ft=ft*12;
			height = ft+in;

		}
		

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.run_tracker, menu);
			return super.onCreateOptionsMenu(menu);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle action bar item clicks here. The action bar will
			// automatically handle clicks on the Home/Up button, so long
			// as you specify a parent activity in AndroidManifest.xml.
			switch (item.getItemId()){
				case R.id.action_pause:
					stopClick(item);
					return true;
				case R.id.action_reset:
					resetClick(item);
					return true;
				case R.id.action_save:
					saveClick(item);
					return true;
				case R.id.action_Home:
					goHome();
					return true;
					
				default:
					return super.onOptionsItemSelected(item);
			}
			
		}

		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		//This Method triggers every time the phone moves (Constantly)
		public void onSensorChanged(SensorEvent arg0) {
			
			//Setting the 3 args X/Y/Z too floats for easier use in vector calculation
			float[] XYZ = new float[3];
			XYZ[0]=arg0.values[0];
			XYZ[1]=arg0.values[1];
			XYZ[2]=arg0.values[2];
			
			
			//Calculates the vector to help account for phone movements that aren't steps
			vector= (int) Math.sqrt(XYZ[0]*XYZ[0]+XYZ[1]*XYZ[1]+XYZ[2]*XYZ[2]);
			
			
			//Works but could be better with a weighted moving average
			if(vector>maxVector || vector<minVector){
					steps++;
			}

			//converts steps to miles walked
			distanceMiles=(((height*0.414)*steps)/12)/mile;				//Takes users height and the steps taken in order to find distance traveled in miles
			resultDistance = String.format("%.2f", distanceMiles);		//Formats to string with a 0.00 format
			distance.setText("Miles: "+resultDistance+" mi");			//Sets distance text box to the distance
			
		
			//Converts distance and time to the speed
			if((timePassed/1000)>=filter) {								//MPH calculated every 5 seconds
				distanceMilesTemp=distanceMiles-distanceMilesOld;		//Sets the temporary variable too distanceMiles and subtracts it from distance miles already used
			    mphTotal = (720*distanceMilesTemp);						//720 is the number that you multiple by 5 to equal 60. Likewise you have to multiply the distance traveled
			    															//in this 5 seconds in order to find how fast they are traveling per hour
				resultSpeed=String.format("%.2f", mphTotal);			//Set to string
				speed.setText("Speed: "+resultSpeed+"MPH");				//output to text box
				filter = filter+5;										//prepares if statement for next 5 seconds
				distanceMilesOld=distanceMilesTemp+distanceMilesOld;	//sets how far we've gone in previously loops to subtract at the start of the loop
			}
			//Converts Distance to calories burned
			caloriesBurned = (int) ((weight*0.75)*distanceMiles);
			calories.setText("Calories: "+caloriesBurned+" cal");
			
		}
		
		
	/*
	 * CODE FOR THE STOPWATCH/TIMER
	 */
		
		
		//Values for Stop watch time started/passed
		private long timePassed,timeStart=0;
		
		//Values for stop watch hours/mins/seconds
		private long seconds,minutes,hours=0;
		
		//Values for stop watch Hours/Mins/Seconds strings
		private String secondsString,minutesString,hoursString="";
		
		//boolean for turning on/off stopwatch
		private boolean stop=false;
		
		//Handler(I'm pretty vague on all of the handler stuff)
		private Handler handle = new Handler();
		
		
		//When you hit the start button
		public void startClick (View view){
			if(stop){
				timeStart = System.currentTimeMillis()-timePassed;
			}
			else{
				timeStart = System.currentTimeMillis();
			}
			minVector=7;								//vector changes so it will start counting steps
			maxVector=13;								//Same
			handle.removeCallbacks(startTimer);
			handle.postDelayed(startTimer, 0);
			
		}
		
		//When you hit the stop button
		public void stopClick (MenuItem item) {
			handle.removeCallbacks(startTimer);
			minVector=0;								//Vector changes so it will stop counting steps
			maxVector=100;								//Same
			stop=true;									//
		}
		
		//When you hit the reset button
		public void resetClick(MenuItem item) {
			stop = false;
			timer.setText("00:00:00");
			steps=0;										//resets steps
			speed.setText("Speed: 0.00MPH");	
			mphTotal=0;										//resets speed
			distanceMilesOld=0;								//resets mile accumulation
			
		}
		
		//Runnable for timer 
		private Runnable startTimer = new Runnable() {
			public void run(){
				timePassed=System.currentTimeMillis()-timeStart;	//Takes system timer and -'s it from timer when we start. The difference is the stop watch counting up
				timerUpdate(timePassed);
				handle.postDelayed(this, 100);
				
			}
		};
		
		//turns milliseconds into right format
		private void timerUpdate (long timeElapsed) {
			
			
			//setting milliseconds to appropriate times Min/Sec/Hours
			//using mod to avoid decimal points in time values
			//Example Time != 2.5:00 instead Time = 2:30
			
			seconds =(timeElapsed/1000)%60;
			minutes =(timeElapsed/60000)%60;
			hours =(timeElapsed/3600000);
			
			//Setting output to string to save formatting
			secondsString=String.valueOf(seconds);
			minutesString=String.valueOf(minutes);
			hoursString=String.valueOf(hours);
			
			// if the mod of seconds = 0 then there are no seconds
			if(seconds==0){
				secondsString="00";
			}
			// if if the mod of seconds is between 1-9 then enter as 0X
			if(seconds>0 && seconds<10) {
				secondsString="0"+seconds;
			}
			
			//Same thing for minutes
			if(minutes==0){
				minutesString="00";
			}
			if(minutes>0 && minutes<10) {
				minutesString="0"+minutes;
			}
			
			//Same thing for hours
			if(hours==0){
				hoursString="00";
			}
			if(hours>0 && hours<10) {
				hoursString="0"+hours;
			}
			//Sets textviews
			timer=(TextView)findViewById(R.id.timer);
			timer.setText(hoursString+":"+minutesString+":"+secondsString);
		}
		
		//Goes Back to Home
		public void goHome(){
			Intent newActivity = new Intent(this, HomeScreenActivity.class);
	    	startActivity(newActivity);
		}
		
		public void saveClick(MenuItem item){
			

			double distDouble = Double.parseDouble(resultDistance);
			SharedPreferences sp = getSharedPreferences(PREFS, 0);
			Editor editor = sp.edit();
			
			//Set the total calories eaten for the day to the total eaten during this activitiy
			caloriesBurnt =Integer.parseInt(sp.getString("calBurnTotal", "0"))+caloriesBurned;
			distanceTotal = Double.parseDouble(sp.getString("distance", "0.00"))+distDouble;
			
			//Get the calories from activity and send it to preferences
			//String calTotal = String.valueOf(caloriesBurned);
			editor.putString("calBurnt", String.valueOf(caloriesBurned));
			editor.putString("distanceRun", String.valueOf(distDouble));
			
			String distanceStore = String.valueOf(distanceTotal);
			String calStore = String.valueOf(caloriesBurnt);
			
			//Send total for the day to preferences
			editor.putString("distance", distanceStore);
			editor.putString("calBurnTotal", calStore);
			editor.commit();

			Toast.makeText(RunTrackerActivity.this, "Data Saved", Toast.LENGTH_LONG).show();
			
			Intent newActivity = new Intent(this, HomeScreenActivity.class);
	    	startActivity(newActivity);
			
		}
	}
			